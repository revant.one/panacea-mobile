import { Component, OnInit } from '@angular/core';
import { Doctor } from '../model/doctor';
import { DailyCallReportService } from '../services/daily-call-report.service';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-doctor-visit-area',
  templateUrl: './doctor-visit-area.page.html',
  styleUrls: ['./doctor-visit-area.page.scss'],
})
export class DoctorVisitAreaPage implements OnInit {

  private tempDoctorList: Array<Doctor>;
  private selectedArea: string;
  constructor(
    private DCRService: DailyCallReportService,
    private route: ActivatedRoute,
    private router: Router,

  ) {
    this.tempDoctorList = this.DCRService.doctorList;
    route.queryParams.subscribe((params) => {
      if (router.getCurrentNavigation().extras.state) {
        this.selectedArea = router.getCurrentNavigation().extras.state.area;
        this.tempDoctorList = DCRService.doctorList.filter((doctor) => {
          if (doctor.area === this.selectedArea) {
            return doctor;
          }
        });
      }
    });
  }

  ngOnInit() {
  }

  ionViewWillEnter() {
    // this.DCRService.getDoctorList()
    //   .subscribe({
    //     next: res => {
    //       res.data.forEach((doctor) => {
    //         let tempDoctor = new Doctor();
    //         tempDoctor.name = doctor.doctor_name;
    //         tempDoctor.area = doctor.area;
    //         tempDoctor.place = doctor.sub_territory;
    //         this.DCRService.doctorList.push(tempDoctor);
    //       });
    //       this.tempDoctorList = this.DCRService.doctorList;
    //     }
    //   });
  }

  navigate(doctor: Doctor) {
    let navigationExtras: NavigationExtras = {
      state: {
        doctor: doctor
      }
    }
    this.router.navigate(['doctor-visit-medicine'], navigationExtras);
  }
}
