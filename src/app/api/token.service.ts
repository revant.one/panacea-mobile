import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { stringify } from 'querystring';
import { switchMap, catchError } from 'rxjs/operators';
import { parse } from 'url';
import {
  InAppBrowser,
  InAppBrowserObject,
} from '@ionic-native/in-app-browser/ngx';
import {
  STATE,
  EXPIRES_IN,
  ACCESS_TOKEN,
  REVOCATION_URL,
  TOKEN_URL,
  CLIENT_ID,
  REFRESH_TOKEN,
  LOGGED_IN,
  ONE_HOUR_IN_SECONDS_STRING,
  ID_TOKEN,
  SCOPE,
  REDIRECT_PREFIX,
} from './constants/storage';
import { StorageService } from './storage.service';
import { FrappeOauth2Config } from './constants/oauth2config.interface';
import { FrappeConfigError } from './constants/frappe-config.error';

export const STATE_LENGTH = 32;

@Injectable({
  providedIn: 'root',
})
export class TokenService {
  state: string;
  authWindow: InAppBrowserObject;
  config: FrappeOauth2Config;

  constructor(
    private http: HttpClient,
    private storage: StorageService,
    private iab: InAppBrowser,
  ) {}

  setupOauthConfig(config: FrappeOauth2Config) {
    this.config = config;
    for (const key of Object.keys(this.config)) {
      this.storage.store(key, this.config[key]);
    }
  }

  initializeCodeGrant() {
    if (!this.config) {
      throw new FrappeConfigError('config missing');
    }

    this.processAuthorizeWindow();
  }

  processAuthorizeWindow() {
    const url = this.generateAuthUrl();

    this.authWindow = this.iab.create(url, '_blank', {
      location: 'no',
      zoom: 'no',
      clearcache: 'yes',
    });

    if (this.authWindow && this.authWindow.on) {
      this.authWindow.on('loadstart').subscribe(event => {
        if (event.url.startsWith(this.config.appUrl)) {
          this.onOauth2Callback(event.url);
        }
      });
      this.authWindow.on('exit').subscribe(event => this.authWindow.close());
    }
  }

  generateAuthUrl() {
    this.state = this.generateState();
    this.storage.store(STATE, this.state);

    let url = this.config.authorizationUrl;
    url += '?scope=' + this.config.scope;
    url += '&response_type=code';
    url += '&client_id=' + this.config.clientId;
    url += '&redirect_uri=' + this.config.appUrl + REDIRECT_PREFIX;
    url += '&state=' + this.state;

    return url;
  }

  generateState() {
    let result = '';
    const characters =
      'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;
    for (let i = 0; i < STATE_LENGTH; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }

  getToken() {
    const expiration = localStorage.getItem(EXPIRES_IN);
    console.log({expiration, EXPIRES_IN})
    if (expiration) {
      const now = new Date();
      const expirationTime = new Date(expiration);

      // expire 20 min early
      expirationTime.setSeconds(expirationTime.getSeconds() - 1200);
      if (now < expirationTime) {
        const accessToken = localStorage.getItem(ACCESS_TOKEN);
        return of(accessToken);
      }
      return this.refreshToken().pipe(
        catchError(error => {
          this.storage.clear(ACCESS_TOKEN);
          this.storage.clear(REFRESH_TOKEN);
          this.storage.clear(LOGGED_IN);
          return of();
        }),
      );
    }
    return of();
  }

  refreshToken() {
    const tokenURL = localStorage.getItem(TOKEN_URL);
    const requestBody = {
      grant_type: 'refresh_token',
      refresh_token: localStorage.getItem(REFRESH_TOKEN),
      redirect_uri: this.config.appUrl + REDIRECT_PREFIX,
      client_id: localStorage.getItem(CLIENT_ID),
      // scope: localStorage.getItem(SCOPE),
    };

    return this.http
      .post<any>(tokenURL, stringify(requestBody), {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
        },
      })
      .pipe(
        switchMap(bearerToken => {
          this.revokeToken(bearerToken.access_token, bearerToken.refresh_token);
          const expirationTime = new Date();
          const expiresIn =
            bearerToken.expires_in || ONE_HOUR_IN_SECONDS_STRING;
          expirationTime.setSeconds(
            expirationTime.getSeconds() + Number(expiresIn),
          );
          this.storage.store(EXPIRES_IN, expirationTime.toISOString());
          this.storage.store(ID_TOKEN, bearerToken.id_token);
          return of(bearerToken.access_token);
        }),
      );
  }

  revokeToken(accessToken?: string, refreshToken?: string) {
    const revocationURL = localStorage.getItem(REVOCATION_URL);
    const oldAccessToken = localStorage.getItem(ACCESS_TOKEN);
    this.http.get(revocationURL + '?token=' + oldAccessToken).subscribe({
      next: success => {
        if (accessToken) {
          this.storage.store(ACCESS_TOKEN, accessToken);
        }
        if (refreshToken) {
          this.storage.store(REFRESH_TOKEN, refreshToken);
        }
      },
      error: error => {},
    });
  }

  onOauth2Callback(callbackUrl) {
    this.storage.clear(STATE);
    const urlParts = parse(callbackUrl, true);
    const query = urlParts.query;
    const code = query.code as string;
    const state = query.state as string;
    const error = query.error;

    if (this.state !== state) {
      return;
    }
    if (error !== undefined) {
      return;
    } else if (code) {
      this.processCode(code);
      this.authWindow.close();
    }
  }

  processCode(code: string) {
    const req: any = {
      grant_type: 'authorization_code',
      code,
      redirect_uri: this.config.appUrl + REDIRECT_PREFIX,
      client_id: localStorage.getItem(CLIENT_ID),
      scope: localStorage.getItem(SCOPE),
    };
    const url = localStorage.getItem(TOKEN_URL);
    this.http
      .post<any>(url, stringify(req), {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
        },
      })
      .subscribe({
        next: response => {
          const expiresIn = response.expires_in || ONE_HOUR_IN_SECONDS_STRING;
          const expirationTime = new Date();
          expirationTime.setSeconds(
            expirationTime.getSeconds() + Number(expiresIn),
          );

          this.storage.store(ACCESS_TOKEN, response.access_token);
          this.storage.store(REFRESH_TOKEN, response.refresh_token);
          this.storage.store(EXPIRES_IN, expirationTime.toISOString());
          this.storage.store(ID_TOKEN, response.id_token);
          this.storage.store(LOGGED_IN, 'true');
        },
        error: error => {
          this.storage.clear(LOGGED_IN);
        },
      });
  }
}
